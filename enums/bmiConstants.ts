export enum bmiConstants {

    UNDER_WEIGHT = -1,
    OK = 0,
    OVERWEIGHT = 1

}