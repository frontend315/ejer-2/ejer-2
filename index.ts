import { patientModel } from "./model/patientModel";

var patients: Array<patientModel> = []

console.log(' Se agregaran 3 nuevos pacientes:')


patients.push(new patientModel('Tania Reyes Gacia', 18, "M", 51, 1.75, "2012-06-25", "CA", "TO"))

console.log('paciente agregado')

patients.push(new patientModel(null, 27, "Masculino", 81, 1.75, null, null, null))

console.log('paciente agregado')

patients.push(new patientModel(null, 27, "Masculino", 81, 1.75, null, null, null))

console.log('paciente agregado')

console.log('se mostrara una lista de los pacientes')


let lista = '';
let id = 1
if (patients.length > 0) {
    patients.forEach(paciente => {
        lista.concat(id.toString(), ' ', paciente.getName(), '\n');
        id++;
    })
} else {
    lista = 'lista vacia';
}

console.log(lista);

console.log('se mostrara al paciente 1')

console.log(patients[0].toString())

console.log('se mostrara al paciente 2')

console.log(patients[1].toString())

console.log('se mostrara al paciente 3')

console.log(patients[2].toString())