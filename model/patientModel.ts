import { person } from "../interfaces/person";
import { genders } from "../enums/genders";
import { bmiConstants } from "../enums/bmiConstants";


export class patientModel implements person {
    
    private name: string;
    private age: number;
    private curp: string;
    private gender: genders;
    private weight: number;
    private height: number;
    private birthday: Date;
    private country: string;
    private state: string;

    constructor(name: string | null, 
                age:number, 
                gender: string | null, 
                weigth:number | null, 
                height:number | null,
                birthday: string | null, 
                country: string | null, 
                state: string | null){
        this.name = name || 'Manuel Alejandro Reyes Ramirez';
        this.age = age;
        this.weight = weigth ||  1.75;
        this.height = height ||  81;
        this.birthday = new Date(birthday || "1993-12-30");
        this.country = country || "MX";
        this.state = state || "DF";
        this.gender = this.checkGender(gender || 'H');
        this.curp = this.createCurp();
    }

    public calculateBMI(): bmiConstants {
        let calc:number = 0;
        let bmi:bmiConstants = bmiConstants.OK;
        
        calc = this.weight/Math.pow(this.height, 2);
        
        if (calc < 20){
            bmi = bmiConstants.UNDER_WEIGHT;
        } else if(calc > 25) {
            bmi = bmiConstants.OVERWEIGHT;
        }

        return bmi;
    }

    public isOlder(): boolean {
        let verify: boolean;
        
        this.age > 18 ? verify = true : verify = false;

        return verify;
    }
    
    private checkGender(gender:string ): genders {
        let genderVerify: genders;
        
        switch (gender){
            case 'woman':
            case 'M':
            case 'm':
            case 'mujer':
            case 'w':
            case 'W':
                genderVerify = genders.M;
                break;
            default:
                genderVerify = genders.H;
                break;
        }

        return genderVerify;
    }

    private createCurp(): string {
        let curpGen='';
        let arrayName = this.name.split(' ');

        curpGen.concat(arrayName[arrayName.length-1][0], arrayName[arrayName.length-1][1], arrayName[arrayName.length-2][0], arrayName[0][0]);
        curpGen.concat(this.birthday.getFullYear.toString(),this.birthday.getMonth.toString(), this.birthday.getDay.toString());
        curpGen.concat(this.gender, this.state, this.makeid(5));
        
        return curpGen;
    }

    private makeid(length:number ):string {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * 
     charactersLength));
       }
       return result;
    }

    public getName(): string {
        return this.name;
    }

    public getAge(): number {
        return this.age;
    }

    public getBirtday(): Date {
        return this.birthday;
    }

    public getCurp(): string {
        return this.curp;
    }

    public getGender(): genders {
        return this.gender;
    }

    public getCountry(): string {
        return this.country
    }

    public getState(): string {
        return this.state;
    }

    public getHeight(): number {
        return this.height;
    }

    public getWeidht(): number {
        return this.weight;
    }

    public setName(name:string): void {
        this.name = name;
    }

    public setAge(age: number): void {
        this.age = age;
    }
    public setBirthday(birthday:Date): void {
        this.birthday = birthday;
    }

    public setGender(gender:string): void {
        this.gender = this.checkGender(gender);
    }

    public setCountry(country: string): void {
        this.country = country.slice(0,1);
    }

    public setState(state:string): void {
       this.state = state.slice(0,1);
    }

    public setHeight(height:number): void {
        this.height = height;
    }

    public setWeidth(weight:number): void {
        this.weight = weight;
    }

    public toString(): string {
        let status: string = '';

        switch (this.calculateBMI()){
            case -1:
                status = 'bajo de peso';
                break;
            case 1:
                status = 'sobre peso';
                break;
            default:
                status = 'peso ideal';
                break;
        }

        return `
            Nombre: ${this.name} 
            Fecha de Nacimineto: ${this.birthday} 
            Edad: ${this.age} años
            Sexo: ${this.gender == genders.M ? 'mujer': 'hombre'}
            CURP: ${this.curp.toUpperCase()}
            Origen: ${this.state.toUpperCase()}, ${this.country.toUpperCase()}
            Peso: ${this.weight}
            Altura: ${this.height}
            BMI: ${status}
        `
    }

}