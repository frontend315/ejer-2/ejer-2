import { genders } from "../enums/genders";

export interface person {
    calculateBMI(): number;
    isOlder(): boolean;
    toString(): string;
    getName(): string;
    getAge(): number;
    getBirtday(): Date
    getCurp(): string;
    getGender(): string
    getCountry(): string;
    getState(): string;
    getHeight(): number;
    getWeidht(): number;
    setName(name: string): void;
    setAge(age: number): void;
    setBirthday(birthday:Date): void
    setGender(gender:genders): void
    setCountry(country:string): void;
    setState(state:string): void;
    setHeight(height:number): void;
    setWeidth(weight:number): void;
}